//Tarea
//1. Transformen a una funcion de flecha
//2. Tiene que retornar un objeto implicito
//3. Pruebas

/* function getUsuarioActivo(nombre){
    return {
        uid: 606688410,
        username = "HernanD",
    }
};

const usuarioActivo = getUsuarioActivo('Hernan');
console.log(usuarioActivo); */

const getUsuarioActivo = (nombre) => 
    (
        console.log(`El nombre es: ${nombre}`),
        {
            uid: 606688410,
            username: 'HernanD'
        }
    );

let nombreU = 'hernan';
const usuarioActivo = getUsuarioActivo(nombreU);
console.log(usuarioActivo);