//Funciones en JS

const saludar = function(nombre){
    return `Hola, ${nombre}`;
}

//Las funciones de flecha tiene el return implicito
const saludar2 = (nombre) => `Hola, ${nombre}`;
const saludar3 = () => `Hola Mundo`;

//console.log(saludar('Hernan'));
//console.log(saludar2('Hernan'));
//console.log(saludar3());

const getUser = () => ({
    uid: 606688410,
    username:'Rimuru-sama',

});
const cuentaGenshin = getUser();
console.log(cuentaGenshin);