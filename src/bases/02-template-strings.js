
// `` estas comillas se llaman Backtick / Back Quote

const nombre = 'Hernan';
const apellido = 'Diaz'

//forma comun concatenada
//const nombreCompleto = nombre + ' ' + apellido;

//forma de template strings
const nombreCompleto = `${nombre} ${apellido}`;

console.log(nombreCompleto);

function getSaludo(nombre){
    return 'Hola ' + nombre;
}

console.log(`Esto es un texto: ${getSaludo(nombre)}`)