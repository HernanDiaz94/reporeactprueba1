const persona = {
    nombre: 'Hernan',
    apellido: 'Diaz',
    edad: 27,
    direccion: {
        ciudad: 'Catamarca, Capital',
        CP: 4700,
        latitud: 14.1111,
        altura: 789,
    }
}

//console.log({persona}); 

/*
Desde EC6 si creamos un objeto nuevo con o sin 
nombre y este tiene una propiedad con el mismo 
nombre de un objeto que definimos antes, solo 
poniendo una sola vez ese nombre haremos referencia 
al objeto antes creado.
*/
let A;
//console.table(A = {persona}); //me muestra el objeto como una tabla
//console.log(persona);

//const persona2 = persona; //persona2 refiere a persona (por su referencia en memoria)
const persona2 = {...persona}; //hago un clon de persona y se lo asigno a persona2
persona2.nombre = "Agustin"; //cambio el nombre en un nuevo objeto sin cambiar a persona original
console.log(persona);
console.log(persona2);