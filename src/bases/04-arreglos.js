//Arreglo en JS

//const arreglo = new Array(10); //array de tamaño fijo pero se le puede agregar items con push().
const arreglo = [1,2,3,4];
/* 
arreglo.push(1);
arreglo.push(2);
arreglo.push(3);
arreglo.push(4); 
*/

//let arreglo2 = arreglo;
//arreglo2.push(5);
let arreglo2 = [...arreglo,5];

//callback es una funcion que se ejecuta en un metodo
const arreglo3 = arreglo2.map(function(numero){
    return numero *2;
});

console.log(arreglo, arreglo2, arreglo3);