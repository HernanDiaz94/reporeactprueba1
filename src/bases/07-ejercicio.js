//Tarea
//1. El primer valor del arr se llamara nombre.
//2. El segundo valor se llamara setNombre.

const holaMundo = ()=>{console.log('Hola Mundo')}

const vseState = (valor) =>{
    return [valor, holaMundo]; //holaMundo es una callback
}

//const arr = vseState('Goku');
// console.log(arr);
//arr[1](); //Hola mundo //formaSinDesestruc

const [nombre, setNombre] = vseState('Goku');
console.log(nombre); //Goku
setNombre(); //Hola mundo //formaConDesestruc


