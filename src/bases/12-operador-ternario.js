//Operador ternario

/* let mensaje='';

if (!activo){
    mensaje = "activo";
}else{
    mensaje= "inactivo";
} */
const activo = true;
//const mensaje = (activo) ? 'activo' : 'inactivo';
//const mensaje = (!activo) ? 'activo' : null;

const mensaje = (activo) && 'Activo';
console.log(mensaje);