
import {heroes} from "../data/heroes";

//console.log(heroes);

/* const getHeroeById = (id) =>{
    //el find recibe una funcion como parametro (callback)
    return  heroes.find((heroe)=>{
        //return true;
        if(heroe.id ===id){
            return true;
        }else{ 
            return false;
        }
    });
}; */

/* const getHeroeById = (id) =>{
    return  heroes.find( (heroe)=> heroe.id === id );
}; */

/*Forma abreviada */
export const getHeroeById=(id)=>heroes.find(heroe=>heroe.id === id);

//console.log (getHeroeById(2));

export const getHeroesByOwner = (owner) => heroes.filter((heroe) => heroe.owner == owner)
//console.log (getHeroesByOwner('Marvel'));