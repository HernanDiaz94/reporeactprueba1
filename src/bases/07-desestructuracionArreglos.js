const personajes = ['Goku','Vegeta','Gohan'];

// console.log(personajes[0]);
// console.log(personajes[1]);
// console.log(personajes[2]);

/*Las comas indican que omiti desestructuran 
el primer y segundo elemento del arreglo 'personajes',
solo nombro a p3 que seria personajes[2] = 'Gohan'. */
const [, ,p3] = personajes; 
console.log(p3);

/* const retornaArreglo = () => {
    return ['ABC',123];
} */

const retornaArreglo = () => ['ABC',123]

const [letras,numeros] = retornaArreglo();
console.log(letras,numeros);

