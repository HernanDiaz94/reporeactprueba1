

/* const promesa = new Promise((resolve,reject)=>{
    setTimeout(() => {
        resolve();
    }, 1000);
}); */

import { getHeroeById } from "./bases/08-import-export";
import { heroes } from "../data/heroes";
/* 
const promesa = new Promise((resolve,reject)=>{
    setTimeout(() => {
        const p1 = getHeroeById(2);
        //console.log(heroe);
        resolve(p1);
        //reject('No se pudo encontrar el heroe');
    }, 1000);
});

promesa.then((heroe)=> {console.log('Heroe:', heroe)})
.catch(err => console.warn(err))
.finally(); */

const getHeroeByIdAsync = (id) =>{
    const promesa = new Promise((resolve,reject)=>{
        setTimeout(() => {
            const p1 = getHeroeById(id);
            //console.log(heroe);
            if(p1){
                resolve(p1);
            }else{
                reject('No se pudo encontrar el heroe');
            }
            
        }, 1000);
    });

    return promesa;
}

getHeroeByIdAsync(1)
        //.then(heroe =>{console.log('Heroe: ', heroe)} )
        .then(console.log) //forma abreviada
        //.catch(err =>console.warn(err))
        .catch(console.warn) //forma abreviada