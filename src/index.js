//Desestructuracion
//Asignacion Desestructurante

const persona = {
    nombre: 'Hernan',
    edad: 27,
    trabajo: 'Programador',
};

// console.log(persona.nombre);
// console.log(persona.edad);
// console.log(persona.trabajo);

/* Uso desestructuracion */
//const {nombre} = persona;
//console.log(nombre);

// const {nombre:nombre2} = persona;
// console.log(nombre2);

/* const retornaPersona = ({nombre,edad,trabajo2='Tecnico'}) => {
    console.log(nombre,edad,trabajo2);
}
retornaPersona(persona); */

const datosOkPersona = ({nombre,edad}) => {
return {nombreOk:nombre,edadOk:edad,fdn:{dia:17, mes: 9, anio:1994}};
};

const {nombreOk, edadOk,/* fdn */ fdn:{dia,mes,anio}} = datosOkPersona(persona); //desestructuro el objeto que devuelve la funcion
//const {dia,mes,anio} = fdn;
console.log(nombreOk,edadOk);
console.log(`${dia}/${mes}/${anio}`);
